package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.Password;
import dao.UserDao;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);

		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
		UserDao userDao = new UserDao();
		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("id", id);
		request.setAttribute("loginId", userDao.findById(id).getLoginId());
		request.setAttribute("name", userDao.findById(id).getName());
		request.setAttribute("birthDate", userDao.findById(id).getBirthDate());
		request.setAttribute("createDate", userDao.findById(id).getCreateDate());
		request.setAttribute("updateDate", userDao.findById(id).getUpdateDate());
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
		return;
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
        // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
        String id = request.getParameter("id");
        String loginId = request.getParameter("login_id");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birth_date");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userDao = new UserDao();

		if(name.isEmpty() || birthDate.isEmpty()){
			//ユーザー名、生年月日が空欄時のエラー処理
        	// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("id", id);
			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);
			// ユーザー情報更新にフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}else {
			if(password1.isEmpty() || password2.isEmpty()) {
				//passwordなしのupdateメソッド
				userDao.update(name, birthDate, id);
			}else {
				if(password1.equals(password2)) {
					//passwordありのupdateメソッド
					String password = Password.Password(password1);
					userDao.update(password, name, birthDate, id);
				}else {
					//passwordが間違っている時の処理
	            	// リクエストスコープにエラーメッセージをセット
	    			request.setAttribute("errMsg", "入力された内容は正しくありません");
	    			request.setAttribute("id", id);
	    			request.setAttribute("loginId", loginId);
	    			request.setAttribute("name", name);
	    			request.setAttribute("birthDate", birthDate);
	    			// ユーザー情報更新にフォワード
	    			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
	    			dispatcher.forward(request, response);
	    			return;
				}
			}
		}

		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("UserListServlet");

	}

}
