package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.Password;
import dao.UserDao;

/**
 * Servlet implementation class UserCreate
 */
@WebServlet("/UserCreateServlet")
public class UserCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserCreateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
        // リクエストパラメータの取得
        String loginId = request.getParameter("login-id");
        String password1 = request.getParameter("password1");
        String password2 = request.getParameter("password2");
        String userName = request.getParameter("user-name");
        String birthDate = request.getParameter("birth-date");
    	UserDao userDao = new UserDao();

        if(loginId.isEmpty() || password1.isEmpty() || userName.isEmpty() || birthDate.isEmpty()) {
        	// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			// ログインjspにフォワード
			request.setAttribute("loginId", loginId);
			request.setAttribute("name", userName);
			request.setAttribute("birthDate", birthDate);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
			return;
        }

        if(userDao.findLoginId(loginId)!=null) {
        	request.setAttribute("errMsg", "入力された内容は正しくありません");

			// ログインjspにフォワード
        	request.setAttribute("loginId", loginId);
			request.setAttribute("name", userName);
			request.setAttribute("birthDate", birthDate);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
			return;
        }


        //作成、更新日時の取得
        Calendar cl = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd hh:mm:ss");


        if(password1.equals(password2)) {
        		String password = Password.Password(password1);
		    	userDao.insert(userDao.newId(), loginId, userName, birthDate, password, sdf.format(cl.getTime()), sdf.format(cl.getTime()));
		        response.sendRedirect("UserListServlet");
        }else {
        	// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			// ログインjspにフォワード
			request.setAttribute("loginId", loginId);
			request.setAttribute("name", userName);
			request.setAttribute("birthDate", birthDate);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
			return;
        }

	}
}