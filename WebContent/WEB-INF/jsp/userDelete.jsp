<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>ユーザ削除画面</title>
  <!-- BootstrapのCSS読み込み -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <!-- オリジナルCSS読み込み -->
  <link href="css/original/common.css" rel="stylesheet">

</head>

<body>

  <!-- header -->
  <header>
    <nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
      <ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="UserListServlet">ユーザ管理システム</a>
        </li>
      </ul>
      <ul class="navbar-nav flex-row">
        <li class="nav-item">
          <a class="nav-link" href="#">${userInfo.name}</a>
        </li>
        <li class="nav-item">
          <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
        </li>
      </ul>
    </nav>
  </header>
  <!-- /header -->

  <!-- body -->
  <div class="container">
    <div class="delete-area">
      <p>ログインID : ${loginId}、名前 : ${name} を消去しますか？</p>
      <div class="row">
        <div class="col-sm-6">
          <a href="UserListServlet" class="btn btn-light btn-block">キャンセル</a>
        </div>
        <div class="col-sm-6">
          <form method="post" name="form_1" action="UserDeleteServlet">
		     <input type="hidden" name="id" value=${id}>
		     <a href="javascript:form_1.submit()" class="btn btn-primary btn-block">OK</a>
		  </form>
        </div>
      </div>
    </div>
  </div>

</body>

</html>